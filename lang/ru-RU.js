export default {
  signIn: 'Вход',
  signUp: 'Регистрация',
  signInError: 'Пароль или логин неверный',
  name: 'Название',
  date: 'Дата',
  'Loading data': 'Загрузка данных',
  'Sport type': 'Тип спорта',
  signUpSuccess: 'Аккаунт успешно создан',
}
