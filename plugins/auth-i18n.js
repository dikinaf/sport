export default ({ app }) => {
  const loginPage = app.localePath({ name: 'login' })
  app.$auth.options.redirect.login = app.$auth.options.redirect.callback =
    loginPage
  app.$auth.options.redirect.home = app.localePath({ name: 'index' })
}
