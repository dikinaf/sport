/* ============
 * vee-validate
 * ============
 *
 * vee-validate is a template-based validation framework for Vue.js that allows you to validate inputs and display
 * errors
 *
 * https://github.com/logaretm/vee-validate
 */

import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'
import { required, min } from 'vee-validate/dist/rules'

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

extend('required', required)
extend('min', min)
