import includes from 'lodash/includes'
import split from 'lodash/split'
import get from 'lodash/get'
import join from 'lodash/join'
import filter from 'lodash/filter'
import map from 'lodash/map'

export const getDataWithIds = (ids, data) =>
  filter((item) => includes((item) => get(item, 'id'), ids), data)

export const getIdsFromUrl = (data) =>
  filter((item) => parseInt(item), split(get(data, 'ids'), ','))

export const prepareDataIdsForUrl = (data) =>
  join(
    map((item) => get(item, 'id'), data),
    ','
  )
