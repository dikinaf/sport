export const state = () => ({})

export const actions = {
  registerNewUserAction(_, params) {
    return this.$axios.post('auth/register/', params)
  },
}
