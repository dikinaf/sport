import get from 'lodash/get'

export const state = () => ({})

export const actions = {
  getGameListAction(_, params) {
    const query = get(this, ['$router', 'history', 'current', 'query'])
    return this.$axios.get('game/games/', { params: { ...query, params } })
  },
  getSportTypeListAction() {
    return this.$axios.get('game/sport-types/')
  },
}
